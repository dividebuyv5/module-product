<?php

declare(strict_types=1);

namespace Dividebuy\Product\Helper;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Common\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
  private StoreConfigHelper $configHelper;

  private Registry $registry;
  private CurrencyFactory $currencyFactory;

  public function __construct(
      Context $context,
      StoreConfigHelper $storeConfigHelper,
      Registry $registry,
      CurrencyFactory $currencyFactory
  ) {
    $this->configHelper = $storeConfigHelper;
    $this->registry = $registry;
    $this->currencyFactory = $currencyFactory;

    parent::__construct($context);
  }

  /**
   * Retrieve product status.
   *
   * @param  null  $storeId
   *
   * @return bool
   */
  public function getProductStatus($storeId = null): bool
  {
    $isIpAllowed = $this->configHelper->isIpAllowed($storeId);

    $isDivideBuyPaymentActive = $this->configHelper->getValue(
        'payment/dbpayment/active',
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
    $productStatus = $this->configHelper->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_ENABLED,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );

    $extensionStatus = $this->configHelper->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_EXTENSION_STATUS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );

    if ($productStatus && $isDivideBuyPaymentActive && $isIpAllowed && $extensionStatus) {
      return true;
    }

    return false;
  }

  /**
   * Function used for checking if softsearch is enabled on product page.
   *
   * @param  null  $storeId
   *
   * @return mixed
   */
  public function isProductSoftSearchEnabled($storeId = null)
  {
    return $this->configHelper->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_SOFT_SEARCH,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve product banner css.
   *
   * @param  null  $storeId
   *
   * @return mixed
   */
  public function getProductBannerCss($storeId = null)
  {
    return $this->configHelper->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_CUSTOM_CSS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve product banner url.
   *
   * @return string
   */
  public function getProductBannerUrl(): string
  {
    return (string) $this->getConfigHelper()->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).
      DivideBuy::DIVIDEBUY_MEDIA_DIR.
      $this->configHelper->getProductBanner();
  }

  /**
   * Retrieve current Product object.
   *
   * @return Product|ProductInterface|null
   */
  public function getProduct(): ?Product
  {
    return $this->registry->registry('current_product');
  }

  public function getConfigHelper(): StoreConfigHelper
  {
    return $this->configHelper;
  }

  /**
   * @return mixed
   */
  public function getSymbol()
  {
    $currentCurrency = $this->configHelper->getStore()->getCurrentCurrencyCode();
    $currency = $this->currencyFactory->create()->load($currentCurrency);

    return $currency->getCurrencySymbol();
  }
}
