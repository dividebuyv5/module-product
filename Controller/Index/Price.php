<?php

namespace Dividebuy\Product\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Price extends Action
{
    protected $productRepository;

    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $selectedOptions = $this->getRequest()->getParam('selected_options'); // Pass the selected options

        try {
            $product = $this->productRepository->getById($productId);
            $configurableProductType = $product->getTypeInstance();
            
            // Logic to get the associated product based on selected options
            $associatedProduct = $configurableProductType->getProductByAttributes($selectedOptions, $product);

            if ($associatedProduct) {
                $price = $associatedProduct->getPrice();
                $finalPrice = $associatedProduct->getFinalPrice();
                $response = [
                    'success' => true,
                    'price' => $price,
                    'final_price' => $finalPrice,
                ];
            } else {
                $response = ['success' => false, 'message' => 'Unable to find the product with selected options'];
            }
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
        }

        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
