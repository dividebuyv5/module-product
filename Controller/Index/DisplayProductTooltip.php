<?php

declare(strict_types=1);

namespace Dividebuy\Product\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class DisplayProductTooltip extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected StoreConfigHelper $configHelper;
  protected CartHelper $cartHelper;
  protected Template $templateBlock;

  private Currency $currency;

  public function __construct(
      Context $context,
      Template $templateBlock,
      Currency $currency,
      StoreConfigHelper $configHelper,
      CartHelper $cartHelper
  ) {
    parent::__construct($context);

    $this->templateBlock = $templateBlock;
    $this->currency = $currency;
    $this->configHelper = $configHelper;
    $this->cartHelper = $cartHelper;
  }

  /**
   * Used get instalment details and load instalmentsDetails.phtml file.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $price = $this->getRequest()->getparam('price');
    $tooltipType = $this->getRequest()->getParam('tooltipType');
    $instalments = $this->configHelper->getInstallments();
    $instalments_ibc = $this->configHelper->getIbcInstallments();
    $instalmentDetails = array_merge($instalments,$instalments_ibc);
    $templateBlock = $this->templateBlock->getLayout()->createBlock(Template::class);
    $financial_calculation = $this->configHelper->getFinanceCalculation();

    if ($tooltipType === 'product') {
      $this->cartHelper->getItemArray();
      $divideBuyProductTotal = $this->cartHelper->getDivideBuyTotal();
      if($financial_calculation == 1){
        $price += $divideBuyProductTotal;
      }
    }

    $modalBlock = $templateBlock->assign(['instalment' => array($instalmentDetails, $price)])
        ->setTemplate('Dividebuy_Product::dividebuy/product/instalments/productTooltip.phtml')
        ->toHtml();

    return $this->getResponse()
        ->setHeader('Content-Type', 'text/html')
        ->setBody($modalBlock);
  }
}
