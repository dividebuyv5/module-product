<?php

declare(strict_types=1);

namespace Dividebuy\Product\Block\Product;

use Dividebuy\Product\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class View extends Template
{
  protected Data $helper;

  public function __construct(Context $context, Data $retailerHelper, array $data = [])
  {
    $this->helper = $retailerHelper;

    parent::__construct($context, $data);
  }

  /**
   * Check that the product is divide buy or not.If it is divide buy, then it returns the array of required attribute.
   *
   * @return array|bool
   */
  public function checkDivideBuy()
  {
    $storeId = $this->helper->getConfigHelper()->getStoreId();
    $product = $this->helper->getProduct();
    $status = $this->helper->getProductStatus($storeId);

    if ($product && $this->helper->getProductStatus($storeId) && $product->getDividebuyEnable()) {
      return [
          'status' => $status,
          'banner_image' => $this->helper->getProductBannerUrl(),
          'custom_css' => $this->helper->getProductBannerCss($storeId),
          'dividebuy_enable' => $product->getDividebuyEnable(),
          'softsearch_enable' => $this->helper->isProductSoftSearchEnabled($storeId),
      ];
    }

    return false;
  }

  /**
   * Used to get maximum Instalment numbers.
   */
  public function getMaxInstalmentKey()
  {
    return $this->helper->getConfigHelper()->getMaximumAvailableInstalments();
  }

  /**
   * Function used for getting background color.
   */
  public function getBannerBackgroundColor(): string
  {
    return (string) $this->helper->getConfigHelper()->getBannerBackgroundColor();
  }
}
